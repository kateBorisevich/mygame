var game = {
	shirt: null,
	currentSrc: null,
	nextSrc: null,
	gameImages: null
}

window.onload = function() {
	var items = document.querySelectorAll('.item');
	for(var i = 0, length = items.length; i < length; i++){
		items[i].addEventListener("click", function(event) {
			var target = event.target;
			playGame(target);
		});
	}
}


function hideElements() {
	var introduction = document.getElementById('introduction');
	var gameSection = document.getElementById('gameSection');
	introduction.classList.add('hide');
	gameSection.classList.remove('hide');

}

function checkChoice(){
	var inputsChoice = document.querySelectorAll('input[type=checkbox]');
	var arrShirts = [];
	var arrDifficulty = [];
	for(var i = 0; i < inputsChoice.length; i++){
		if(inputsChoice[i].name === "shirt"){
			arrShirts.push(inputsChoice[i]);
		} else {
			arrDifficulty.push(inputsChoice[i]);	
		}
	}

	var conterCheckShirts = 0;
	var conterCheckDifficulty = 0;
	var activeShirt = null;
	var activeDifficulty = null;

	for(var i = 0; i < arrShirts.length; i++){
		if(arrShirts[i].checked === true){
			conterCheckShirts++;
			activeShirt = arrShirts[i];
		}
	}

	if(conterCheckShirts !== 1){
		alert ("Please, check one shirt");
		return;
	}

	for(var i = 0; i < arrDifficulty.length; i++){
		if(arrDifficulty[i].checked === true){
			conterCheckDifficulty++;
			activeDifficulty = arrDifficulty[i];
		}
	}

	if(conterCheckDifficulty !== 1){
		alert ("Please, check one difficulty");
		return;
	}

	hideElements();
	var resArrayImages = createGride(activeShirt,activeDifficulty);
	var gameArrayImges = resArrayImages;
	game.gameImages = gameArrayImges;
}

function createGride(shirt,difficulty){
	var container = document.querySelector('.container');
	var items = document.querySelectorAll('.item');
	var arr = [];
	var neededItems = [];
	if (difficulty.value === 'easy') {
		for(var i = 16; i < items.length; i++){
			devareElements(container,items[i]);
		}
		container.classList.add('easy');
		arr = [1,2,3,4,5,6,7,8];
	} else if (difficulty.value === 'hard'){
		for(var i = 20; i < items.length; i++){
			devareElements(container,items[i]);
		}
		container.classList.add('hard');
		arr = [1,2,3,4,5,6,7,8,9,10];	
	} else {
		container.classList.add('very-hard');
		arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18];
	}

	
	if(shirt.value === 'pica'){
		items.forEach(function(item){
			item.classList.add('selectedShirt1');
			game.shirt = 'url(./img/pikachu.png)';
		});
	} else if (shirt.value === 'compositePicture') {
		items.forEach(function(item){
			item.classList.add('selectedShirt2');
			game.shirt = 'url(./img/pokemons.png)';
		});
	} else {
		items.forEach(function(item){
			item.classList.add('selectedShirt3');
			game.shirt = 'url(./img/pokeBall.png)';
		});
	}
	var arrImg = createBackgroundImages(arr);
	var arrImages = arrImg;
	var newArrayImages = mixArr(arrImages);
	var mixArrayImages = newArrayImages;
	return mixArrayImages;
}

function mixArr(array) {
 	for (var i = array.length - 1; i > 0; i--) {
 		var j = Math.floor(Math.random() * (i + 1));
 		var temp = array[i];
 		array[i] = array[j];
 		array[j] = temp;
 	}
 	return array;
}

function createBackgroundImages(array){
	var imgRoot = './img/background/';
	var arrImages = [];
	for(var i = 1, length = array.length; i <= length; i++){
		arrImages.push(imgRoot + i);
	}
	return arrImages.concat(arrImages);
}

function devareElements (parentElem,elem) { 
	var devareElem = parentElem.removeChild(elem);
}


function playGame(item) {
	if(item.classList.contains('open')) {
		return;	
	} 
	var gameImages = game.gameImages;
	var container = document.querySelector('.container');
	var items = document.querySelectorAll('.item');
	var currentImageSrc = game.currentSrc;
	var nextImageSrc = game.nextSrc;
	if(currentImageSrc === null){
		currentImageSrc = openImage(item,gameImages,items);
		game.currentSrc = currentImageSrc;
	} else {
		nextImageSrc = openImage(item,gameImages,items);
		game.nextSrc = nextImageSrc;
	}
	if (currentImageSrc != null && nextImageSrc != null) {
		chekImagesSrc();
	}
	return;
}


function openImage(item,arrImg,items){
	//item.classList.add('selected');
	var shirt = game.shirt;
	var strUrl = null;
	for(var i = 0, length = items.length; i < length; i++){
		if(items[i] == item){
			strUrl = 'url('  + arrImg[i] + '.png)';
			item.classList.add('open');
			item.classList.add('emptyBackground');
			item.style.backgroundImage = strUrl;	
			break;
		}
	}
	return strUrl;
}

function chekImagesSrc(){
	if(game.currentSrc !== null && game.nextSrc !== null){
		compareSrc(game.currentSrc,game.nextSrc);
	}
}

function compareSrc(src1,src2){
	var itemsClassOpen = document.querySelectorAll('.open');
	var shirt = game.shirt;
	itemsClassOpen[0].classList.remove('open');
	itemsClassOpen[1].classList.remove('open');

	if(src1 == src2) {
		setTimeout(function() {
			itemsClassOpen[0].style.visibility = 'hidden';
			itemsClassOpen[1].style.visibility = 'hidden';
		}, 1000);

	} else {
		setTimeout(function() {
			itemsClassOpen[0].classList.remove('emptyBackground');
			itemsClassOpen[1].classList.remove('emptyBackground');
			itemsClassOpen[0].style.backgroundImage = shirt;
			itemsClassOpen[1].style.backgroundImage = shirt;
		}, 1000);
	}

	game.currentSrc = null;
	game.nextSrc = null;
	return;
}

